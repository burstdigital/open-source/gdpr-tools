<?php

namespace GdprTools\Database;

use Doctrine\DBAL\DBALException;
use Faker\Factory as FakerFactory;
use GdprTools\Configuration\Configuration;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class Anonymizer
 *
 * @package GdprTools\Database
 */
class Anonymizer
{

    private const OPERATOR_IS = '=';

    private const OPERATOR_IS_NOT = '!=';

    /**
     * Anonymize database based on the configuration.
     */
    public function anonymize(Configuration $configuration, SymfonyStyle $io): void
    {

        if ( ! $locale = $configuration->toArray()['locale']) {
            $locale = 'en_US';
        }

        $config = $configuration->toArray()['anonymize'];
        if ( ! is_array($config)) {
            throw new RuntimeException('Config does not contain tables in the configuration.');
        }

        $database   = new Database($configuration);
        $connection = $database->getConnection();

        $tables = array_keys($config['tables']);

        foreach ($tables as $table) {
            // Check for exclude in this table
            $exclude = $config['exclude'][$table] ?? [];

            $columns = $config['tables'][$table];

            if ( ! is_array($columns)) {
                throw new RuntimeException(
                    $table.' does not contain columns in the configuration.'
                );
            }

            $result = $connection->query('SELECT * FROM '.$table);

            while ($row = $result->fetch()) {

                $headers = array_keys($row);

                $values = $row;
                foreach ($columns as $column => $data) {
                    if ( ! in_array($column, $headers)) {
                        throw new RuntimeException($column.' does not exist in the database.');
                    }

                    if ( ! isset($config['tables'][$table][$column]['formatter'])) {
                        throw new RuntimeException($column.' does not have a formatter.');
                    }

                    $formatter = $config['tables'][$table][$column]['formatter'];

                    $args = $config['tables'][$table][$column]['args'] ?? [];
                    if (isset($config['tables'][$table][$column]['args'])) {
                        $args = $config['tables'][$table][$column]['args'];
                    }

                    $unique = (bool)($config['tables'][$table][$column]['unique'] ?? false);

                    $faker = FakerFactory::create($locale);
                    if ($unique) {
                        $faker->unique();
                    }
                    $values[$column] = call_user_func_array([$faker, $formatter], $args);

                }

                $set   = $this->prepareSet($headers, $values);
                $where = $this->prepareWhere($row, $exclude);
                $sql   = 'UPDATE '.$table.' SET '.$set.' WHERE '.$where.';';

                //                $io->writeln($sql);

                $connection->query($sql);

            }

            $io->success('Successfully anonymized '.$table.'.');
        }
    }

    /**
     * Prepares the SET for a database update query.
     *
     * @param  array  $headers
     * @param  array  $values
     *
     * @return string
     */
    protected function prepareSet(array $headers, array $values)
    {
        $set = [];

        foreach ($headers as $header) {
            $value = $this->prepareValue($values[$header], '=');

            array_push($set, '`'.$header.'` '.$value.'');
        }

        return implode(', ', $set);
    }

    /**
     * Prepares a value for a database query.
     *
     * @param  mixed  $value
     *
     * @param $operator
     * @param  bool  $isWhere
     *
     * @return string
     */
    protected function prepareValue($value, $operator, $isWhere = false)
    {
        if ($value === null) {
            $value = 'NULL';

            if ($isWhere && $operator === $this::OPERATOR_IS) {
                $operator = 'IS';
            } elseif ($isWhere && $operator === $this::OPERATOR_IS_NOT) {
                $operator = 'IS NOT';
            }
        } elseif ( ! is_int($value)) {
            $value = '\''.str_replace(["'", '"'], '', $value).'\'';
        }

        return $operator.' '.$value;
    }

    /**
     * Prepares a where statement for a database query.
     *
     * @param  array  $row
     * @param  array  $exclude
     *
     * @return string
     */
    protected function prepareWhere(array $row, array $exclude)
    {
        $headers = array_keys($row);

        $where = [];

        foreach ($headers as $header) {
            $value = $this->prepareValue($row[$header], '=', true);

            array_push($where, '`'.$header.'` '.$value.'');
        }

        $excludeHeaders = array_keys($exclude);

        foreach ($excludeHeaders as $excludeHeader) {
            foreach ($exclude[$excludeHeader] as $value) {
                $value = $this->prepareValue($value, '!=', true);

                array_push($where, '`'.$excludeHeader.'` '.$value.'');
            }
        }

        return implode(' AND ', $where);
    }

}
