<?php

namespace GdprTools\Database;

use Doctrine\DBAL\DBALException;
use GdprTools\Configuration\Configuration;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class Truncator
 *
 * @package GdprTools\Database
 */
class Truncator
{

    public function truncate(Configuration $configuration, SymfonyStyle $io)
    {
        if ( ! $configuration->isAvailable(['truncate'], true, true)) {
            $io->warning('truncate does not contain tables in the configuration.');
        }

        $tables = $configuration->toArray()['truncate'];

        $database   = new Database($configuration);
        $connection = $database->getConnection();

        foreach ($tables as $table) {
            try {
                $connection->query('TRUNCATE TABLE '.$table);
                $io->success($table.' truncated.');
            } catch (\Exception $e) {
                $io->warning('Table ' . $table . ' could not be truncated');
            }
        }
    }

}
