<?php

namespace GdprTools\Database;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use GdprTools\Configuration\Configuration;

/**
 * Class Database
 *
 * @package GdprTools\Database
 */
class Database
{

    /** @var Connection Connection */
    protected $connection;

    public function __construct(Configuration $configuration)
    {
        $configuration->isAvailable(
            [
                'database' => [
                    'scheme',
                    'host',
                    'port',
                    'name',
                    'user',
                    'password',
                ],
            ],
            true,
            true
        );

        $databaseConfiguration = new \Doctrine\DBAL\Configuration();

        $database = $configuration->toArray()['database'];

        $connectionParams = [
            'dbname'   => $database['name'],
            'user'     => $database['user'],
            'password' => $database['password'],
            'host'     => $database['host'],
            'port'     => $database['port'],
            'driver'   => $database['scheme'],
        ];

        $this->connection = DriverManager::getConnection($connectionParams, $databaseConfiguration);
    }

    public function getConnection(): Connection
    {
        return $this->connection;
    }

}


