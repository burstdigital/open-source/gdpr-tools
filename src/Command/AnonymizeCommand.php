<?php

namespace GdprTools\Command;

use Exception;
use GdprTools\Configuration\Configuration;
use GdprTools\Database\Anonymizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class AnonymizeCommand
 *
 * @package GdprTools\Command
 */
class AnonymizeCommand extends Command
{

    private const ARGUMENT_FILE = 'file';

    protected function configure(): void
    {
        $this
            ->setName('db:anonymize')
            ->setDescription('anonymizes a database based on a yaml configuration.')
            ->setHelp('anonymizes a database based on a yaml configuration.')
            ->addArgument(
                self::ARGUMENT_FILE,
                InputArgument::REQUIRED,
                'Where is the yaml configuration located?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $file = $input->getArgument(self::ARGUMENT_FILE);

        $configuration = new Configuration($file);

        try {
            $anonymizer = new Anonymizer();
            $anonymizer->anonymize($configuration, $io);
        } catch (Exception $e) {
            $output->writeln($e->getMessage());
        }

        return 0;
    }

}
