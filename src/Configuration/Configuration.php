<?php

namespace GdprTools\Configuration;

use Platformsh\ConfigReader\Config;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Yaml\Yaml;

class Configuration
{

    /** @var array Configuration */
    private $configuration;

    public function __construct(string $file)
    {
        $this->configuration = Yaml::parseFile($file);
        $this->mergeConfigWithPresets();
        $this->replaceEnvVars();
    }

    /**
     * Adds all presets to the configuration array.
     */
    private function mergeConfigWithPresets(): self
    {
        $presets = $this->configuration['presets'] ?? [];

        foreach ($presets as $preset) {
            $presetConfig        = Yaml::parseFile(__DIR__.'/Presets/'.$preset.'.yml');
            $this->configuration = $this->array_merge_recursive_distinct(
                $presetConfig,
                $this->configuration
            );
        }

        return $this;
    }

    private function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset ($merged [$key]) && is_array($merged [$key])) {
                $merged [$key] = $this->array_merge_recursive_distinct($merged [$key], $value);
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    private function setEnvVar(string $name, string $value): void {
        if (!putenv("$name=$value")) {
            throw new \RuntimeException('Failed to create environment variable: ' . $name);
        }
        $order = ini_get('variables_order');
        if (stripos($order, 'e') !== false) {
            $_ENV[$name] = $value;
        }
        if (stripos($order, 's') !== false) {
            if (strpos($name, 'HTTP_') !== false) {
                throw new \RuntimeException('Refusing to add ambiguous environment variable ' . $name . ' to $_SERVER');
            }
            $_SERVER[$name] = $value;
        }
    }

    /**
     * Replaces env vars in the configuration with actual env var values.
     */
    private function replaceEnvVars(): void
    {

        /**
         * Check if this is an Platform.sh project and make the db relationship available
         * as env vars
         */
        if (getenv('PLATFORM_RELATIONSHIPS')) {
            $platformConfig = new Config();
            if ($platformConfig->hasRelationship('database')) {
                $credentials = $platformConfig->credentials('database');
                $this->setEnvVar('DB_CONNECTION', 'pdo_' . $credentials['scheme']);
                $this->setEnvVar('DB_HOST', $credentials['host']);
                $this->setEnvVar('DB_PORT', $credentials['port']);
                $this->setEnvVar('DB_NAME', $credentials['path']);
                $this->setEnvVar('DB_USER', $credentials['username']);
                $this->setEnvVar('DB_PASSWORD', $credentials['password']);
            }
        }

        array_walk_recursive(
            $this->configuration,
            static function (&$value) {
                if (
                    strpos($value, '${') === 0 &&
                    substr($value, -1, strlen('}')) === '}'
                ) {
                    $tempValue = $value;

                    $tempValue = substr($tempValue, strlen('${'));
                    $tempValue = substr($tempValue, 0, strlen($tempValue) - strlen('}'));

                    $envValue = getenv($tempValue);

                    if ($envValue === false) {
                        throw new RuntimeException(
                            $tempValue.' is not an existing environment variable.'
                        );
                    }

                    $value = $envValue;
                }
            }
        );
    }

    /**
     * Returns the yaml configuration as an array.
     */
    public function toArray(): array
    {
        return $this->configuration;
    }

    /**
     * Checks if all provided keys are available in the configuration.
     * Supports nested keys by passing a nested array as parameter.
     */
    public function isAvailable(
        array $keys,
        bool $printErrors = true,
        bool $dieOnErrors = false,
        array $configuration = null,
        string $nesting = ''
    ): bool {
        if ($configuration === null) {
            $configuration = $this->configuration;
        }

        $isAvailable = true;

        // Check all keys that are not an array
        foreach ($keys as $key) {
            if ( ! is_array($key) && ! array_key_exists($key, $configuration)) {
                $isAvailable = false;

                if ($printErrors) {
                    throw new RuntimeException($nesting.$key.' does not exist in configuration.');
                }
            }
        }

        // Check all keys that are an array (and recursively check the array items)
        foreach (array_keys($keys) as $key) {
            if (is_array($keys[$key])) {
                if ( ! array_key_exists($key, $configuration)) {
                    $isAvailable = false;

                    if ($printErrors) {
                        throw new RuntimeException(
                            $nesting.$key.' does not exist in configuration.'
                        );
                    }
                } else {
                    $nestedAvailable = $this->isAvailable(
                        $keys[$key],
                        $printErrors,
                        false,
                        $configuration[$key],
                        $key.':'.$nesting
                    );

                    if ( ! $nestedAvailable) {
                        $isAvailable = false;
                    }
                }
            }
        }

        if ( ! $isAvailable && $dieOnErrors) {
            die();
        }

        return $isAvailable;
    }

}
