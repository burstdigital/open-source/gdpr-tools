# GDPR Tools

## Installation

```
composer require burst/gdpr-tools
```

## Usage

```
php vendor/bin/gdpr-tools <command>
```

## Commands

| Command | Description | Arguments |
| --- | --- | --- |
| `db:anonymize` | anonymizes database based on a yaml configuration. | __file__ - A yaml configuration file, must end with `.yml`. See _Configuration_ for details. |
| `db:truncate` | Truncates database tables based on a yaml configuration. | __file__ - A yaml configuration file, must end with `.yml`. See _Configuration_ for details. |

## Configuration

### Anonimization

GDPR Tools uses Faker [https://github.com/fzaninotto/Faker](https://github.com/fzaninotto/Faker) to anonimize data.
This replaces production and personalized data not with obfuscated, unreadable data, but with dummy data which still has a readable and valid format.

For each table in the database, columns can be configured with a [Faker formatter](https://github.com/fzaninotto/Faker#formatters).


```yaml
anonimize:
  tables:
    {{table_name}}:
    {{column_name}}:
      # Faker Formatter (required) 
      formatter: {{fakerFormatter}}
      # Unique value (optional, default false)
      unique: true
      # Array of arguments to be passes to the Faker Formatter (optional)
      args: 
        - 200
```

### Truncate
Just a list of tables which need to be truncated
```yaml
truncate:
  - table_a
  - table_b 
```


### Example

```yaml
presets:
  - drupal8

locale: nl_NL

database:
  scheme: pdo_mysql
  host: ${DB_HOST}
  port: 3306
  name: ${DB_NAME}
  user: ${DB_USER}
  password: ${DB_PASSWORD}

truncate:
  - event_log_track
  - z_tickets_log

anonymize:
  tables:
    user__field_description:
      field_description_value:
        formatter: realText
        args:
          - 300
        unique: true
    user__field_facebook:
      field_facebook_value:
        formatter: url
    user__field_google:
      field_google_value:
        formatter: url
    user__field_instagram:
      field_instagram_value:
        formatter: url
    user__field_name:
      field_name_value:
        formatter: name
    user__field_phone:
      field_phone_value:
        formatter: phoneNumber
    user__field_twitter:
      field_twitter_value:
        formatter: url

  exclude:
    users_field_data:
      uid:
        - 0 # super admin
        - 1 # admin
        - 5 # webmaster
```

### Presets
Presets can be used to load pre-configured tables which need to be truncated and anonymized.  
Current available presets are:
- [drupal8](/src/Configuration/Presets/drupal8.yml)

Configuration set in the local .yml configuration are merged with the preset config.

### Locale
Text formatters can differ per locale. Any locale supported by Faker can be set.
```yaml
locale: nl_NL
```


